package pravo.securecoding.beans;

public class Books {
	private int bookID;
	private String title;
	private String author;
	private float price;
	private String description;
	
	public Books() {
		
	}

	public Books(int bookID, String title, String author, float price, String description) {
		this.bookID = bookID;
		this.title = title;
		this.author = author;
		this.price = price;
		this.description = description;
	}
	
	public Books(String title, String author, float price, String description) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.description = description;
	}

	public int getBookID() {
		return bookID;
	}

	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
