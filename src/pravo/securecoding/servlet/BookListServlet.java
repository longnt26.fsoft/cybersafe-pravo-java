package pravo.securecoding.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pravo.securecoding.beans.Books;
import pravo.securecoding.utils.DBUtils;
import pravo.securecoding.utils.MyUtils;

@WebServlet(urlPatterns = {"/bookList"})
public class BookListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public BookListServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(req);
		
		String errorString = null;
		List<Books> list = null;
		try {
			list = DBUtils.queryBook(conn);
		} catch (Exception e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		//Store information to the request before forward to view
		req.setAttribute("errorString", errorString);
		req.setAttribute("bookList", list);
		
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bookListView.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
}
