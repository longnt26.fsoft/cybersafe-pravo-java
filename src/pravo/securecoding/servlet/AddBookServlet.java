package pravo.securecoding.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pravo.securecoding.beans.Books;
import pravo.securecoding.utils.DBUtils;
import pravo.securecoding.utils.MyUtils;

@WebServlet(urlPatterns = {"/addBook"})
public class AddBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public AddBookServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/views/addBookView.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(req);
		
		String title = (String) req.getParameter("title");
		String author = (String) req.getParameter("author");
		String priceStr = (String) req.getParameter("price");
		String description = (String) req.getParameter("description");
		float price = 0;
		try {
			price = Float.parseFloat(priceStr);
		} catch (Exception e) {
		}
		
		Books book = new Books(title, author, price, description);
		
		String errorString = null;
		
		String regex = "^[a-zA-Z0-9_ ]*$";
		
		if(title == null || !title.matches(regex)) {
			errorString = "Book Title invalid!";
		} else if (author == null || !author.matches(regex)) {
			errorString = "Author invalid!";
		} else if (price == 0.0 || !priceStr.matches("[+-]?([0-9]*[.])?[0-9]+")) {
			errorString = "Price invalid!";
		} else if (!description.matches(regex)) {
			errorString = "Description invalid!";
		}
		
		if (errorString == null) {
			try {
				DBUtils.insertBook(conn, book);
			} catch (Exception e) {
				e.printStackTrace();
				errorString = e.getMessage();
			}	
		}
		
		req.setAttribute("errorString", errorString);
		req.setAttribute("book", book);
		
		if (errorString != null) {
			RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/addBookView.jsp");
			dispatcher.forward(req, resp);
		} else {
			resp.sendRedirect(req.getContextPath() + "/bookList");
		}
	}
}
