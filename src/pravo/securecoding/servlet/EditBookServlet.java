package pravo.securecoding.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pravo.securecoding.beans.Books;
import pravo.securecoding.utils.DBUtils;
import pravo.securecoding.utils.MyUtils;

@WebServlet(urlPatterns = { "/editBook" })
public class EditBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public EditBookServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(req);
		
		int bookID = Integer.parseInt(req.getParameter("bookID"));
		
		Books book = null;
		String errorString = null;
		
		try {
			book = DBUtils.findBook(conn, bookID);
		} catch (Exception e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		
		if (errorString != null && book == null) {
			resp.sendRedirect(req.getServletPath() + "/bookList");
			return;
		}
		
		req.setAttribute("errorString", errorString);
		req.setAttribute("book", book);
		
		RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/views/editBookView.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(req);
		
		String title = (String) req.getParameter("title");
		String author = (String) req.getParameter("author");
		String priceStr = (String) req.getParameter("price");
		String description = (String) req.getParameter("description");
		float price = 0;
		try {
			price = Float.parseFloat(priceStr);
		} catch (Exception e) {
		}
		
		Books book = new Books(title, author, price, description);
		
		String errorString = null;
		
		try {
			DBUtils.updateBook(conn, book);
		} catch (Exception e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		
		req.setAttribute("errorString", errorString);
		req.setAttribute("book", book);
		
		if (errorString != null) {
			RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("WEB-INF/views/editBookView.jsp");
			dispatcher.forward(req, resp);
		} else {
			resp.sendRedirect(req.getContextPath() + "/bookList");
		}
	} 
}
