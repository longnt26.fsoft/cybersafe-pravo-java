package pravo.securecoding.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pravo.securecoding.beans.Users;
import pravo.securecoding.utils.DBUtils;
import pravo.securecoding.utils.MyUtils;

@WebServlet(urlPatterns= {"/changePassword"})
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public ChangePasswordServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(req);
		
		int userID = Integer.parseInt(req.getParameter("userID"));		
		Users user = null;		
		String errorString = null;
		
		try {
			user = DBUtils.findUserByID(conn, userID);
		} catch (Exception e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		
		if (errorString != null && user == null) {
			resp.sendRedirect(req.getServletPath() + "/");
			return;
		}
		
		req.setAttribute("errorString", errorString);
		req.setAttribute("user", user);
		
		RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/views/changePasswordView.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(req);
		
		String username = (String) req.getParameter("username");
		String newPass = (String) req.getParameter("newPass");
		String confirmPass = (String) req.getParameter("confirmPass");
		
		Users user = new Users(username, newPass);
		
		String errorString = null;
		
		try {
			if (newPass.equals(confirmPass)) {
				DBUtils.changePassword(conn, user);
			} else {
				errorString = "New pass and Confirm pass does not match! Try again!";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		
		req.setAttribute("errorString", errorString);
		req.setAttribute("user", user);
		
		if(errorString != null) {
			RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/views/changePasswordView.jsp");
			dispatcher.forward(req, resp);	
		} else {
			resp.sendRedirect(req.getContextPath() + "/userProfile");
		}
	}
}
