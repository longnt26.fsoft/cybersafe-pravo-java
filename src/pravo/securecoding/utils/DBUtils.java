package pravo.securecoding.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pravo.securecoding.beans.Users;
import pravo.securecoding.beans.Books;

public class DBUtils {
	//User's Utils
	public static Users findUser (Connection conn, String username, String password) throws SQLException {
		String query = "Select UserID, Username, Password, Name, Phone, Address from Users "
				+ "     where Username = ? and Password = ?";
		PreparedStatement pstm = conn.prepareStatement(query);
		pstm.setString(1, username);
		pstm.setString(2, password);
		ResultSet rs = pstm.executeQuery();
		
		if(rs.next()) {
			int userID = rs.getInt("UserID");
			String name = rs.getString("Name");
			String phone = rs.getString("Phone");
			String address = rs.getString("Address");
			Users user = new Users();
			user.setUsername(username);
			user.setPassword(password);
			user.setUserID(userID);
			user.setName(name);
			user.setPhone(phone);
			user.setAddress(address);
			return user;
		}
		return null;
	}
	public static Users findUserByName (Connection conn, String username) throws SQLException {
String query = "Select * from Users where Username = ?";
		
		PreparedStatement pstm = conn.prepareStatement(query);
		pstm.setString(1, username);
		ResultSet rs = pstm.executeQuery();
		
		if (rs.next()) {
			int userID = rs.getInt("UserID");
			String password = rs.getString("Password");
			String name = rs.getString("Name");
			String phone = rs.getString("Phone");
			String address = rs.getString("Address");
			Users user = new Users();
			user.setUsername(username);
			user.setPassword(password);
			user.setUserID(userID);
			user.setName(name);
			user.setPhone(phone);
			user.setAddress(address);
			return user;
		}		
		return null;
	}
	public static Users findUserByID (Connection conn, int userID) throws SQLException {
		String query = "Select * from Users where UserID = ?";
		
		PreparedStatement pstm = conn.prepareStatement(query);
		pstm.setInt(1, userID);
		ResultSet rs = pstm.executeQuery();
		
		if (rs.next()) {
			String username = rs.getString("Username");
			String password = rs.getString("Password");
			String name = rs.getString("Name");
			String phone = rs.getString("Phone");
			String address = rs.getString("Address");
			Users user = new Users();
			user.setUsername(username);
			user.setPassword(password);
			user.setUserID(userID);
			user.setName(name);
			user.setPhone(phone);
			user.setAddress(address);
			return user;
		}		
		return null;
	}
	
	public static void changePassword (Connection conn, Users user) throws SQLException {
		String query = "Update Users set Password = ? where Username = ?";
		PreparedStatement pstm = conn.prepareStatement(query);
		pstm.setString(1, user.getPassword());
		pstm.setString(2, user.getUsername());
		pstm.executeUpdate();
	}
	
	//Book Utilities
	public static List<Books> queryBook(Connection conn) throws SQLException {
		String query = "Select * from Books";
		PreparedStatement pstm = conn.prepareStatement(query);
		
		ResultSet rs = pstm.executeQuery();
		List<Books> list = new ArrayList<Books>();
		while(rs.next()) {
			int bookID = rs.getInt("BookID");
			String title = rs.getString("Title");
			String author = rs.getString("Author");
			float price = rs.getFloat("Price");
			String description = rs.getString("Description");
			Books book = new Books();
			book.setBookID(bookID);
			book.setTitle(title);
			book.setAuthor(author);
			book.setPrice(price);
			book.setDescription(description);
			list.add(book);
		}
		return list;
	}
	
	public static Books findBook(Connection conn, int bookID) throws SQLException {
		String query = "Select * from Books where BookID = ?";
		
		PreparedStatement pstm = conn.prepareStatement(query);
		pstm.setInt(1, bookID);
		
		ResultSet rs = pstm.executeQuery();
		
		while(rs.next()) {
			String title = rs.getString("Title");
			String author = rs.getString("Author");
			float price = rs.getFloat("Price");
			String description = rs.getString("Description");
			Books book = new Books(bookID, title, author, price, description);
			return book;
		}
		return null;
	}
	
	public static void updateBook(Connection conn, Books book) throws SQLException {
		String query = "Update Books set Title = ?, Author = ?, Price = ?, Description = ? where BookID = ?";
		PreparedStatement pstm = conn.prepareStatement(query);
		
		pstm.setString(1, book.getTitle());
		pstm.setString(2, book.getAuthor());
		pstm.setFloat(3, book.getPrice());
		pstm.setString(4, book.getDescription());
		pstm.setInt(5, book.getBookID());
		pstm.executeUpdate();
	}
	
	public static void insertBook(Connection conn, Books book) throws SQLException {
		String query = "insert into Books(Title, Author, Price, Description) values (?, ?, ?, ?)";
		PreparedStatement pstm = conn.prepareStatement(query);
		pstm.setString(1, book.getTitle());
		pstm.setString(2, book.getAuthor());
		pstm.setFloat(3, book.getPrice());
		pstm.setString(4, book.getDescription());
		
		pstm.executeUpdate();
	}
	
	public static void deleteBook(Connection conn, int bookID) throws SQLException {
		String query = "delete from Books where BookID = ?";
		PreparedStatement pstm = conn.prepareStatement(query);
		
		pstm.setInt(1, bookID);
		
		pstm.executeUpdate();
	}
} 
