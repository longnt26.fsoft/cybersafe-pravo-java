package pravo.securecoding.utils;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pravo.securecoding.beans.Users;

public class MyUtils {
	public static final String ATT_NAME_CONNECTION = "ATTRIBUTE_FOR_CONNECTION";
	 
	private static final String ATT_NAME_USER_NAME = "ATTRIBUTE_FOR_STORE_USER_NAME_IN_COOKIE";
	 
	//Store Connection to the attribute of requests - only available request time till responsed ot user's browser
	public static void storeConnection(ServletRequest request, Connection conn) {
		request.setAttribute(ATT_NAME_CONNECTION, conn);
	}
	
	//Get the object Connection stored in request's attribute
	public static Connection getStoredConnection(ServletRequest request) {
		Connection conn = (Connection) request.getAttribute(ATT_NAME_CONNECTION);
		return conn;
	}
	
	//Stored logged in user information to session
	public static void storeLoginedUser (HttpSession session, Users loginedUser) {
		//Can access to the user's info in JSP -> ${loginedUser}
		session.setAttribute("loginedUser", loginedUser);
	}
	
	//Get info of user in session
	public static Users getLoginedUser(HttpSession session) {
		Users loginedUser = (Users) session.getAttribute("loginedUser");
		return loginedUser;
	}
	
	//Store users information to cookie
	public static void storeUserCookie(HttpServletResponse response, Users user) {
		System.out.println("Store user cookie");
		Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, user.getUsername());
		cookieUserName.setMaxAge(24 * 60 * 60);
		response.addCookie(cookieUserName);
	}
	
	public static String getUserNameInCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (ATT_NAME_USER_NAME.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
	//Delete user's Cookie 
	public static void deleteUserCookie(HttpServletResponse response) {
		Cookie cookieUsername = new Cookie(ATT_NAME_USER_NAME, null);
		cookieUsername.setMaxAge(0);
		response.addCookie(cookieUsername);
	}
}
