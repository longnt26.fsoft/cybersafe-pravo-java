<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book List</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Books List</h3>
				</div>
				<div class="card-body col-lg-10" >
					<a class="btn btn-success" href="addBook">Add New Book</a>
					<p style="color: red;">${errorString}</p>
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<tr>
								<th>Book ID</th>
								<th>Title</th>
								<th>Author</th>
								<th>Price</th>
								<th>Description</th>
								<th>Action</th>						
							</tr>
							<c:forEach items="${bookList}" var="book">
								<tr>
									<td>${book.bookID}</td>
									<td>${book.title}</td>
									<td>${book.author}</td>
									<td>${book.price }</td>
									<td>${book.description }</td>
									<td><a href="editBook?bookID=${ book.bookID }" class="btn btn-warning">Edit</a>
									<a href="deleteBook?bookID=${ book.bookID }" class="btn btn-danger">Delete</a>
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>