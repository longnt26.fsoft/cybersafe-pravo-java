<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
      	<meta charset="UTF-8">
      	<title>Login</title>
      	<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
   
    	<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    
    	<link href="css/sb-admin.css" rel="stylesheet">
   </head>
   <body class="bg-light">
 	<div class="container" style="margin-bottom: 20px;">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header"><b>CYBERSAFE</b> - SAVE ME</div>
            <div class="card-body">
                <form method="POST" action="${pageContext.request.contextPath}/login">
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="text" name="username" id="username" value= "${user.username}" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
                            <label for="userName">Username</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="password" name="password" id="password" value= "${user.password}" class="form-control" placeholder="Password" required="required">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="form-group">
                            <input type="checkbox" name="rememberMe" value= "Y">
                            <label for="password">Remember Me</label>                      
                    </div>
					<p style="color: red;">${errorString}</p>
                    <input class="btn btn-primary btn-block" type="submit" value="Login" />
                </form>
            </div>
            <p style="color:blue; text-align: center;">User Name: longnt, password: admin</p>
        </div>
    </div>
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span style="color: white;">Copyright © CyberSafe Secure Coding for Web Application Project - PRAVO 2019</span>
        </div>
    </div>
   </body>
  
</html>