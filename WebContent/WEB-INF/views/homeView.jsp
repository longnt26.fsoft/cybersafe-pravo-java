<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home Page</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body id="page-top">

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
	<jsp:include page="_menu.jsp"></jsp:include>
	<div id="content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-x1-12" style="margin-left: 20px;">
					<h3>Home Page</h3>

					This is demo Save Me web application using <b>JSP, Servlet &amp; JDBC.</b>
					<br> <br> <b>It includes the following functions:</b>
					<ul>
						<li>Login</li>
						<li>Storing user information in cookies</li>
						<li>Product List</li>
						<li>Create Product</li>
						<li>Edit Product</li>
						<li>Delete Product</li>
					</ul>
					<br>
					<b>It also includes the following security vulnerabilities:</b>
					<ul>
						<li>A1 - Injection <p style="color: white">SQL Injection - Login function</p></li>
						<li>A2 - Broken Authentication & Session Management <p style="color: white">Valid user session after logout - Logout function</p></li>
						<li>A3 - Cross Site Scripting - XSS <p style="color: white">Stored XSS - Create Product function</p></li>
						<li>A4 - Insecure Direct Object References - IDOR <p style="color: white">Change Password function</p></li>
						<li>A8 - Cross Site Request Forgery - CSRF <p style="color: white">Change Password function</p></li>
					</ul>
				</div>
			</div>
		</div>
		<jsp:include page="_footer.jsp"></jsp:include>
	</div>
	</div>
</body>
</html>

