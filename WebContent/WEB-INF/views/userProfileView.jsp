<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User Info</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Hello, ${user.name}</h3>
				</div>
				<div class="card-body">
					<div class="col-md-5">
					User Name: <b>${user.username}</b>
					</div>
					<div class="col-md-5">
					Phone: ${user.phone }
					</div>
					<div class="col-md-5">
					Address: ${user.address }
					</div>
					<br /> <br> 
					<a class="btn btn-outline-info" href="changePassword?userID=${user.userID}">Change Password</a>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>