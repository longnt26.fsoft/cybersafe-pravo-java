<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
  
<!-- Sidebar -->
  <ul class="sidebar navbar-nav">
      <li class="nav-item active">
          <a class="nav-link" href="${pageContext.request.contextPath}/bookList">
              <i class="fas fa-fw fa-book"></i>
              <span>Manage Books</span>
          </a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/userProfile">
              <i class="fas fa-fw fa-user"></i>
              <span>Profile</span>
          </a>
      </li>
  </ul>
	