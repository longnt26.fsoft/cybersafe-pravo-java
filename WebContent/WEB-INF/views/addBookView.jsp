<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add New Book</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Add New Book</h3>
				</div>
				<div class="card-body">
					<p class="text-danger">${errorString}</p>

					<form method="POST"
						action="${pageContext.request.contextPath}/addBook">
						<div class="form-horizontal">

							<div class="form-group">
								<label class="control-label col-md-4">Title</label> <input
									class="form-control col-md-10" type="text" name="title"
									value="${fn:escapeXml(book.title)}" />
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Author</label> <input
									class="form-control col-md-10" type="text" name="author"
									value="${fn:escapeXml(book.author)}" />
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Price</label> <input
									class="form-control col-md-10" type="text" name="price"
									value="${fn:escapeXml(book.price)}" />
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Description</label> <input
									class="form-control col-md-10" type="text" name="description"
									value="${fn:escapeXml(book.description)}" />
							</div>
							<div class="form-group">
								<div class="col-md-offset-2 col-md-10">
									<input class="btn btn-primary" type="submit" value="Add" /> 
									<a class="btn btn-default" href="bookList">Cancel</a>
									</td>
								</div>
							</div>

						</div>
					</form>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>